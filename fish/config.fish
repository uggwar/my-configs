set -x PATH $HOME/src/zls/zig-out/bin $PATH
set -x PATH $HOME/.cargo/bin $PATH
set -x PATH $HOME/.local/bin $PATH
set -x PATH $HOME/local/neovim/bin $PATH
set -x PATH $HOME/local/go/bin $PATH
set -x PATH $HOME/local/zig $PATH
set -x PATH $HOME/local/rofi/bin $PATH
set -x PATH $HOME/bin $PATH

set -x GOPATH $HOME/go
set -x PATH $GOPATH/bin $PATH

alias reboot='systemctl reboot -i'
alias poweroff='systemctl poweroff -i'
alias fwc='sudo firewall-cmd'
alias r='ranger'
alias vi='nvim'
alias vim='nvim'

set -x EDITOR nvim

set -x _SILENT_JAVA_OPTIONS -Dawt.useSystemAAFontSettings=on
set -e _JAVA_OPTIONS

set -x QT_QPA_PLATFORMTHEME qt5ct

set fish_greeting

direnv hook fish | source

# Vulkan SDK
set -x VULKAN_SDK "$HOME/local/1.3.268.0/x86_64"
set -x PATH $VULKAN_SDK/bin $PATH
set -x LD_LIBRARY_PATH $VULKAN_SDK/lib $LD_LIBRARY_PATH
set -x VK_ADD_LAYER_PATH $VULKAN_SDK/etc/vulkan/explicit_layer.d $VK_ADD_LAYER_PATH

# Kanagawa Fish shell theme
# A template was taken and modified from Tokyonight:
# https://github.com/folke/tokyonight.nvim/blob/main/extras/fish_tokyonight_night.fish
set -l foreground DCD7BA normal
set -l selection 2D4F67 brcyan
set -l comment 727169 brblack
set -l red C34043 red
set -l orange FF9E64 brred
set -l yellow C0A36E yellow
set -l green 76946A green
set -l purple 957FB8 magenta
set -l cyan 7AA89F cyan
set -l pink D27E99 brmagenta

# Syntax Highlighting Colors
set -g fish_color_normal $foreground
set -g fish_color_command $cyan
set -g fish_color_keyword $pink
set -g fish_color_quote $yellow
set -g fish_color_redirection $foreground
set -g fish_color_end $orange
set -g fish_color_error $red
set -g fish_color_param $purple
set -g fish_color_comment $comment
set -g fish_color_selection --background=$selection
set -g fish_color_search_match --background=$selection
set -g fish_color_operator $green
set -g fish_color_escape $pink
set -g fish_color_autosuggestion $comment

# Completion Pager Colors
set -g fish_pager_color_progress $comment
set -g fish_pager_color_prefix $cyan
set -g fish_pager_color_completion $foreground
set -g fish_pager_color_description $comment
