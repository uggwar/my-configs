HISTSIZE=1000
SAVEHIST=1000
HISTFILE=~/.history

path+=("$HOME/.cargo.bin")
path+=("$HOME/neovim/bin")
export PATH

autoload -Uz compinit
compinit

zstyle ':completion:*' menu select

alias vim="nvim"
alias vi="nvim"

eval "$(starship init zsh)"
