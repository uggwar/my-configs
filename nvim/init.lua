_G.uggwar = {}
uggwar.default_compile_path = vim.fn.stdpath("data") .. "/packer_compiled.lua"

local impatient_ok, impatient = pcall(require, "impatient")
if impatient_ok then impatient.enable_profile() end

local packer_ok, _ = pcall(require, "packer")
if not packer_ok then
  local install_path = vim.fn.stdpath("data") .. "/site/pack/packer/start/packer.nvim"
  vim.fn.delete(install_path, "rf")
  vim.fn.system({ "git", "clone", "--depth", "1", "https://github.com/wbthomason/packer.nvim", install_path })
  vim.cmd.packadd "packer.nvim"
  packer_ok, _ = pcall(require, "packer")
  if not packer_ok then vim.api.nvim_err_writeln("Failed to load packer at:" .. install_path) end
end

if packer_ok then
  -- try to execute the compiled packer function
  local run_me, _ = loadfile(uggwar.default_compile_path)
  if run_me then
    run_me()
  else
    vim.api.nvim_err_writeln("Someone needs to run :PackerSync!")
  end
end

vim.opt.guifont = { "FiraCode Nerd Font", ":h12" }

for _, source in ipairs {
  "uggwar.options",
  "uggwar.plugins",
  "uggwar.mappings",
} do
  local status_ok, fault = pcall(require, source)
  if not status_ok then vim.api.nvim_err_writeln("Failed to load " .. source .. "\n\n" .. fault) end
end
