local keymap = require("uggwar.keymap")

keymap.map("n", "<F5>", "<esc>:split term://cargo r<cr>")
keymap.map("n", "<F6>", "<esc>:split term://cargo test<cr>")
