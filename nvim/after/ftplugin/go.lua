local keymap = require("uggwar.keymap")

keymap.map("n", "<F5>", "<esc>:split term://go run .<cr>")
keymap.map("n", "<F6>", "<esc>:split term://go test .<cr>")
