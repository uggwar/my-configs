local uggwar_plugins = {
  -- Packer can manage itself
  ["wbthomason/packer.nvim"] = {},
  -- Optimiser
  ["lewis6991/impatient.nvim"] = {},
  -- Some handy lua functions
  ["nvim-lua/plenary.nvim"] = { module = "plenary" },
  ["EdenEast/nightfox.nvim"] = {
    config = function() require "configs.colorscheme" end,
  },
  ["ellisonleao/gruvbox.nvim"] = {
    config = function() require "configs.colorscheme" end,
  },
  -- Enable jk escape without lag
  ["nvim-zh/better-escape.vim"] = {},
  -- Enable null-ls
  ["jose-elias-alvarez/null-ls.nvim"] = {
    config = function() require "configs.null-ls" end,
  },
  -- Enable prettier for code formatting
  ["MunifTanjim/prettier.nvim"] = {
    config = function() require "configs.prettier" end,
  },
  -- Automatically handle brackets
  ["windwp/nvim-autopairs"] = {
    config = function() require("nvim-autopairs").setup {} end,
  },
  -- Syntax highlighting
  ["nvim-treesitter/nvim-treesitter"] = {
    run = ":TSUpdate",
    event = "BufEnter",
    cmd = {
      "TSInstall",
      "TSInstallInfo",
      "TSInstallSync",
      "TSUninstall",
      "TSUpdate",
      "TSUpdateSync",
      "TSDisableAll",
      "TSEnableAll",
    },
    config = function() require "configs.treesitter" end,
  },
  -- Rust development
  ["simrat39/rust-tools.nvim"] = {},
  -- Zig development
  ["ziglang/zig.vim"] = {},
  -- Tpope magic tricks
  ["tpope/vim-fugitive"] = {},
  ["tpope/vim-surround"] = {},
  ["tpope/vim-commentary"] = {},
  -- Fuzzy finder
  ["nvim-telescope/telescope.nvim"] = {
    cmd = "Telescope",
    module = "telescope",
    config = function() require "configs.telescope" end,
  },
  -- Fuzzy finder syntax support
  [("nvim-telescope/telescope-%s-native.nvim"):format(vim.fn.has "win32" == 1 and "fzy" or "fzf")] = {
    after = "telescope.nvim",
    run = vim.fn.has "win32" ~= 1 and "make" or nil,
    config = function() require("telescope").load_extension(vim.fn.has "win32" == 1 and "fzy_native" or "fzf") end,
  },
  -- NVim LSP config
  ["neovim/nvim-lspconfig"] = {
    config = function() require("configs.lspconfig") end,
  },
  -- LspKind
  ["onsails/lspkind.nvim"] = {},
  -- For completion use cmp
  ["hrsh7th/cmp-nvim-lsp"] = {},
  ["hrsh7th/cmp-buffer"] = {},
  ["hrsh7th/cmp-path"] = {},
  ["hrsh7th/cmp-cmdline"] = {},
  ["hrsh7th/cmp-nvim-lua"] = {},
  ["hrsh7th/nvim-cmp"] = {
    config = function() require "configs.cmp" end,
  },
  -- Snippets
  ["L3MON4D3/LuaSnip"] = {},
  ["saadparwaiz1/cmp_luasnip"] = {},
  ["rafamadriz/friendly-snippets"] = {},
  -- Icons
  ["kyazdani42/nvim-web-devicons"] = {
    module = "nvim-web-devicons",
    config = function() require "configs.nvim-web-devicons" end,
  },
  -- Statusline
  ["nvim-lualine/lualine.nvim"] = {
    config = function() require "configs.lualine" end,
  },
}

local status_ok, packer = pcall(require, "packer")
if status_ok then
  packer.startup {
    function(use)
      for key, plugin in pairs(uggwar_plugins) do
        if type(key) == "string" and not plugin[1] then plugin[1] = key end
        use(plugin)
      end
    end,
    config = {
      compile_path = uggwar.default_compile_path,
      auto_clean = true,
      compile_on_sync = true,
    }
  }
end
