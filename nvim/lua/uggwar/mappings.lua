local map = vim.keymap.set

local function set_mappings(map_table, base)
  -- iterate over the first keys for each mode
  for mode, maps in pairs(map_table) do
    -- iterate over each keybinding set in the current mode
    for keymap, options in pairs(maps) do
      -- build the options for the command accordingly
      if options then
        local cmd = options
        local keymap_opts = base or {}
        if type(options) == "table" then
          cmd = options[1]
          keymap_opts = vim.tbl_deep_extend("force", options, keymap_opts)
          keymap_opts[1] = nil
        end
        -- extend the keybinding options with the base provided and set the mapping
        map(mode, keymap, cmd, keymap_opts)
      end
    end
  end
end

local maps              = { i = {}, n = {}, v = {}, t = {},[""] = {} }

maps[""]["<Space>"]     = "<Nop>"

maps.n["<leader>jj"]    = { "<esc>:Ex<cr>" }
maps.n["<leader><esc>"] = { "<esc>:bd<cr>" }
maps.n["<F12>"]         = { "<esc>:split term://fish<cr>" }

maps.n["<C-p>"]         = { function() require("telescope.builtin").git_files() end, desc = "Search with git files" }
maps.n["<leader>fg"]    = { function() require("telescope.builtin").live_grep() end, desc = "Live Grep" }
maps.n["<leader>ff"]    = { function() require("telescope.builtin").find_files() end, desc = "Search files" }
maps.n["<leader>fb"]    = { function() require("telescope.builtin").buffers() end, desc = "Search buffers" }
maps.n["<leader>fh"]    = { function() require("telescope.builtin").help_tags() end, desc = "Search help" }
maps.n["<leader>fm"]    = { function() require("telescope.builtin").marks() end, desc = "Search marks" }
maps.n["<leader>fo"]    = { function() require("telescope.builtin").oldfiles() end, desc = "Search history" }
maps.n["<leader>sh"]    = { function() require("telescope.builtin").help_tags() end, desc = "Search help" }
maps.n["<leader>sm"]    = { function() require("telescope.builtin").man_pages() end, desc = "Search man" }
maps.n["<leader>sr"]    = { function() require("telescope.builtin").registers() end, desc = "Search registers" }
maps.n["<leader>sk"]    = { function() require("telescope.builtin").keymaps() end, desc = "Search keymaps" }
maps.n["<leader>sc"]    = { function() require("telescope.builtin").commands() end, desc = "Search commands" }
maps.n["<leader>sd"]    = { function() require("telescope.builtin").diagnostics() end, desc = "Search diagnostics" }

set_mappings(maps)
